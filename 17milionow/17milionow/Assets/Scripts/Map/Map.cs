﻿using NaughtyAttributes;
using UnityEngine;

[CreateAssetMenu(menuName = "Map")]
public class Map : ScriptableObject
{
    [Header("Related scene found by gameobject name")]
    public string mapName;
    public Sprite mapImage;
    public Sprite mapImageLocked;

    [Slider(1, 5)]
    public int difficulty = 1;

    public bool isBonusMap;

    public Map previousMap;
    public Map nextMap;

    [EnableIf("isBonusMap")]
    public int bowsNeeded;

    public bool isUnlocked;
    public bool isCompleted;
    public int bowsFound;
    public int bowsMax;

    public Chapter Chapter
    {
        get
        {
            Chapter[] chapters = Resources.LoadAll<Chapter>("Chapters");
            foreach (Chapter chapter in chapters)
            {
                if (chapter.maps.Contains(this) || chapter.bonusMaps.Contains(this))
                    return chapter;
            }
            return null;
        }
    }
}
