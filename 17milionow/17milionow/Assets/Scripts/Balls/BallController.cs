﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class BallController : MonoBehaviour
{
    protected const float WAKE_UP_TRESHOLD = 2f;

    public float speed;
    public float detectionRange;
    public bool isSleeping;

    public Sprite spriteAsleep;
    public Sprite spriteAwaken;
    public GameObject sleepParticleHolder;

    // Use this for initialization
    void Start()
    {
        if (isSleeping)
        {
            GetComponent<SpriteRenderer>().sprite = spriteAsleep;
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
        else
            WakeUp();
    }

    protected void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }

    protected void OnEnable()
    {
        if(isSleeping)
            PlayerController.TransmitPositionAction += CheckDistanceToPlayer;
        else
            InputHandler.OnAccelerate += Move;
    }

    protected void OnDisable()
    {
        PlayerController.TransmitPositionAction -= CheckDistanceToPlayer;
        InputHandler.OnAccelerate -= Move;
    }

    protected void CheckDistanceToPlayer(Vector2 playerPosition)
    {
        if(Vector2.Distance(transform.position, playerPosition) <= detectionRange)
        {
            WakeUp();
        }
    }

    protected virtual void Move(float input)
    {
        if (GetComponent<Renderer>().IsVisibleFrom(Camera.main))
        {
            Vector2 forceVector = Vector2.right * input * speed;
            GetComponent<Rigidbody2D>().AddForce(forceVector, ForceMode2D.Impulse);
        }
    }

    protected void WakeUp()
    {
        isSleeping = false;
        GetComponent<SpriteRenderer>().sprite = spriteAwaken;
        GetComponent<Rigidbody2D>().isKinematic = false;
        Destroy(sleepParticleHolder);
        PlayerController.TransmitPositionAction -= CheckDistanceToPlayer;
        InputHandler.OnAccelerate += Move;
    }
}