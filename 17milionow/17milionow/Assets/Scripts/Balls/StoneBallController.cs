﻿using UnityEngine;

public class StoneBallController : BallController
{
    public int damage;
    public float knockback;
    public Sprite spriteSad;

    private readonly float velocityToDetectHit = 3f;
    private readonly float angularModifier = 5f;
    private readonly float upliftModifier = 5f;
    private readonly float collisionMagnitudeToDamage = 350f;
    private readonly float rotationToSadden = 110f; // <180f
    private bool isSad = false;

    private void FixedUpdate()
    {
        if (isSleeping && GetComponent<Rigidbody2D>().velocity.magnitude > WAKE_UP_TRESHOLD)
            WakeUp();
        else if (!isSad && !isSleeping && transform.rotation.eulerAngles.z > rotationToSadden
            && transform.rotation.eulerAngles.z < 360f - rotationToSadden)
        {
            isSad = true;
            GetComponent<SpriteRenderer>().sprite = spriteSad;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        float collisionMagnitude = collision.relativeVelocity.magnitude * GetComponent<Rigidbody2D>().mass;
        if (collision.collider.GetComponent<DestroyableWall>() != null
            && collisionMagnitude > collision.collider.GetComponent<DestroyableWall>().GetThreshold()
            && GetComponent<Rigidbody2D>().velocity.magnitude > velocityToDetectHit)
        {
            collision.collider.GetComponent<DestroyableWall>().BeforeDestroy();
        }
        else if (collision.collider.GetComponent<IDestroyable>() != null 
            && collision.collider.GetComponent<IDestroyableExplosive>() == null
            && collisionMagnitude > collisionMagnitudeToDamage
            && GetComponent<Rigidbody2D>().velocity.magnitude > velocityToDetectHit)
        {
            collision.collider.GetComponent<IDestroyable>().BeforeDestroy();
        }
    }

    protected override void Move(float input)
    {
        if (GetComponent<Renderer>().IsVisibleFrom(Camera.main))
        {
            Vector2 forceVector = Vector2.right * input * speed;
            GetComponent<Rigidbody2D>().AddForce(forceVector, ForceMode2D.Impulse);
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * Mathf.Abs(input) * upliftModifier, ForceMode2D.Impulse);
            GetComponent<Rigidbody2D>().angularVelocity -= input * angularModifier;
        }
    }
}
