﻿using UnityEngine;

public class SpikeBallController : BallController, IOnTouch, IDestroyable
{
    public int damage;
    public float knockback;

    public GameObject explosionParticle;

    private void FixedUpdate()
    {
        if (isSleeping && GetComponent<Rigidbody2D>().velocity.magnitude > WAKE_UP_TRESHOLD)
            WakeUp();
    }

    public void OnTouch(PlayerController player)
    {
        player.GiveDamage(damage);
        player.KnockBack(knockback, transform.position);
    }

    public void BeforeDestroy()
    {
        Instantiate(explosionParticle, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
