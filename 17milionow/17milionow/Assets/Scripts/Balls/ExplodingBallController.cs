﻿using System.Collections;
using UnityEngine;
using NaughtyAttributes;

public class ExplodingBallController : BallController, IClickable, IDestroyableExplosive
{
    public Sprite spriteAngry;

    public int playerDamage;
    public float explosionPower = 100f;
    public float timeToExplode = 3f;

    public GameObject explosionParticle;
    public GameObject sparklesParticle;
    public Transform sparklesParticleHolder;
    public AudioSource fuseSound;

    private readonly float explosionRadius = 20f;
    private readonly float upliftModifier = 3f;

    private bool isExploding = false;

    [Button]
    public void Clicked()
    {
        if(!isSleeping)
            StartCoroutine(BeginCountdown());
    }

    private IEnumerator BeginCountdown()
    {
        PreExplosionVisuals();
        yield return new WaitForSeconds(timeToExplode);

        Explode();
    }

    private void PreExplosionVisuals()
    {
        Instantiate(sparklesParticle, sparklesParticleHolder);
        GetComponent<SpriteRenderer>().sprite = spriteAngry;
        fuseSound.Play();
    }

    [Button]
    private void Explode()
    {
        if (!isExploding)
        {
            isExploding = true;
            Instantiate(explosionParticle, transform.position, Quaternion.identity);
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);
            foreach (Collider2D hit in colliders)
            {
                if (hit != GetComponent<Collider2D>())
                {
                    if (hit.GetComponent<IDestroyable>() != null)
                    {
                        hit.GetComponent<IDestroyable>().BeforeDestroy();
                    }
                    else if (hit.GetComponent<PlayerController>() != null)
                    {
                        hit.GetComponent<PlayerController>().GiveDamage(playerDamage, false);
                        hit.GetComponent<Rigidbody2D>().AddExplosionForce(explosionPower, transform.position, explosionRadius, upliftModifier);
                    }
                    else if (hit.GetComponent<Rigidbody2D>() != null)
                    {
                        hit.GetComponent<Rigidbody2D>().AddExplosionForce(explosionPower, transform.position, explosionRadius, upliftModifier);
                    }
                }
            }
            MusicPlayer.Instance.explosionSound.Play();
            Destroy(gameObject);
        }
    }

    public void BeforeDestroy()
    {
        Explode();
        Destroy(gameObject);
    }
}
