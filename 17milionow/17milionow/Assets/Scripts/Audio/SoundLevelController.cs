﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundLevelController : MonoBehaviour
{
    public AudioMixer mixer;

    public Image musicSlider;
    public Image soundSlider;

    public Sprite offSprite;
    public Sprite onSprite;

    private static bool isMusicActive = true;
    private static bool isSoundActive = true;

    private void Start()
    {
        if (isMusicActive)
        {
            musicSlider.sprite = onSprite;
        }
        else
        {
            musicSlider.sprite = offSprite;
        }
        if (isSoundActive)
        {
            soundSlider.sprite = onSprite;
        }
        else
        {
            soundSlider.sprite = offSprite;
        }
    }

    public void SetMusic()
    {
        if (isMusicActive)
        {
            mixer.SetFloat("musicVolume", -80f);
            musicSlider.sprite = offSprite;
            isMusicActive = false;
        }
        else
        {
            mixer.SetFloat("musicVolume", 0f);
            musicSlider.sprite = onSprite;
            isMusicActive = true;
        }
    }

    public void SetSound()
    {
        if (isSoundActive)
        {
            mixer.SetFloat("soundVolume", -80f);
            soundSlider.sprite = offSprite;
            isSoundActive = false;
        }
        else
        {
            mixer.SetFloat("soundVolume", 0f);
            soundSlider.sprite = onSprite;
            isSoundActive = true;
        }
    }
}
