﻿using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public AudioSource[] clips;

    private static MusicPlayer instance;
    public int currentClip = 0;

    public AudioSource pickupSound;
    public AudioSource hurtSound;
    public AudioSource hitSound;
    public AudioSource pumpingSound;
    public AudioSource explosionSound;
    public AudioSource pressurePlateSound;
    public AudioSource popSound;

    public static MusicPlayer Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MusicPlayer>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != instance)
                Destroy(gameObject);
        }
    }

    private void Start()
    {
        instance.clips[instance.currentClip].Play();
    }

    private void Update()
    {
        if(Camera.main != null)
        {
            instance.transform.position = Camera.main.transform.position;
        }
        foreach (AudioSource clip in instance.clips)
        {
            if (clip.isPlaying || clip.time != 0f)
                return;
        }
        if (instance.currentClip >= instance.clips.Length - 1)
            instance.currentClip = 0;
        else
            instance.currentClip++;
        instance.clips[instance.currentClip].Play();
    }
}
