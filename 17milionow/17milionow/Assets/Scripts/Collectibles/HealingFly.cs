﻿using UnityEngine;

public class HealingFly : Collectible, IOnTouch
{
    public int healing;

    public void OnTouch(PlayerController player)
    {
        if(player.Health < PlayerController.MAX_HEALTH)
        {
            player.Heal(Mathf.Clamp(healing, 0, PlayerController.MAX_HEALTH - player.Health));
            Instantiate(collectedParticle, transform.position, Quaternion.identity);
            MusicPlayer.Instance.pickupSound.Play();
            Destroy(gameObject);
        }
    }
}
