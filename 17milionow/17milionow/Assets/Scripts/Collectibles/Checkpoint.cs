﻿using UnityEngine;

public class Checkpoint : Collectible, IOnTouch
{
    public GameObject deactivatedHolder;
    public GameObject activatedHolder;

    private bool wasActivated = false;
    private float animationSwitchTime;

    public void OnTouch(PlayerController player)
    {
        if (!wasActivated)
        {
            Instantiate(collectedParticle, transform.position, Quaternion.identity);
            animationSwitchTime = deactivatedHolder.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime;
            deactivatedHolder.SetActive(false);
            activatedHolder.SetActive(true);
            activatedHolder.GetComponent<Animator>().Play("Firefly_Idle", 0, animationSwitchTime);
            player.OnCheckpointCollected(transform.position);
            MusicPlayer.Instance.pickupSound.Play();
            wasActivated = true;
        }
    }
}
