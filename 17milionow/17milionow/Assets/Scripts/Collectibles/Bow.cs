﻿using System;
using UnityEngine;
using NaughtyAttributes;

public class Bow : Collectible, IOnTouch
{
    public static event Action<string> BowCollected = delegate { };

    public string id;

    private void Start()
    {
        if (PlayerPrefs.HasKey(id))
        {
            Destroy(gameObject);
        }
    }

#if UNITY_EDITOR
    private void OnEnable()
    {
        AssignId();
    }
#endif

    private void Awake()
    {
        AssignId();
    }

    public void OnTouch(PlayerController player)
    {
        BowCollected(id);
        Instantiate(collectedParticle, transform.position, Quaternion.identity);
        MusicPlayer.Instance.pickupSound.Play();
        Destroy(gameObject);
    }

    private void AssignId()
    {
        if(string.IsNullOrEmpty(id))
            id = IdsHolder.GenerateUniqueID(gameObject);
    }

    [Button]
    private void ResetId()
    {
        id = IdsHolder.GenerateUniqueID(gameObject);
    }
}
