﻿using UnityEngine;

public class GigaFly : Collectible, IOnTouch
{
    public void OnTouch(PlayerController player)
    {
        player.IncreaseChances();
        Instantiate(collectedParticle, transform.position, Quaternion.identity);
        MusicPlayer.Instance.pickupSound.Play();
        Destroy(gameObject);
    }
}
