﻿using UnityEngine;

public abstract class Collectible : MonoBehaviour
{
    public GameObject collectedParticle;
}
