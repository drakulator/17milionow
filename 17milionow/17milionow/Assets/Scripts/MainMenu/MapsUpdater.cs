﻿using UnityEngine;

public class MapsUpdater : MonoBehaviour
{
    private void Awake()
    {
        Chapter[] chapters = Resources.LoadAll<Chapter>("Chapters");
        Map[] maps = Resources.LoadAll<Map>("Maps");
        foreach (Map map in maps)
        {
            if (PlayerPrefs.HasKey(map.name))
            {
                if (PlayerPrefs.GetInt(map.name) == 1)
                {
                    map.isUnlocked = true;
                }
                if (PlayerPrefs.GetInt(map.name) == 2)
                {
                    map.isUnlocked = true;
                    map.isCompleted = true;
                }
            }
            if (PlayerPrefs.HasKey(map.name + "_Bows"))
            {
                map.bowsFound = PlayerPrefs.GetInt(map.name + "_Bows");
            }
            if (map.isBonusMap)
            {
                if (PlayerPrefs.GetInt("GlobalBowsFound") >= map.bowsNeeded)
                {
                    map.isUnlocked = true;
                }
            }
        }
        if (!PlayerPrefs.HasKey("GlobalBowsFound"))
        {
            PlayerPrefs.SetInt("GlobalBowsFound", 0);
        }
        foreach (Chapter chapter in chapters)
        {
            if (chapter.nextChapter != null && chapter.IsNextChapterUnlocked())
                chapter.nextChapter.startMap.isUnlocked = true;
        }
    }
}
