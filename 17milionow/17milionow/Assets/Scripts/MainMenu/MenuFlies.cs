﻿using UnityEngine;

public class MenuFlies : MonoBehaviour
{
    public float speed;

    private Vector2 destination;

    private readonly float timeBetweenMoves = 1f;
    private readonly float adMargin = 80f;

    private void Start()
    {
        destination = transform.position;
        InvokeRepeating("Fly", Random.Range(0f, timeBetweenMoves), timeBetweenMoves);
    }

    // Update is called once per frame
    void Update ()
    {
        transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);
	}

    private void Fly()
    {
        destination = Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0f, Screen.width), Random.Range(Mathf.Clamp(adMargin, 0f, Screen.height), Screen.height)));
    }
}
