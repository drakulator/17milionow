﻿using GoogleMobileAds.Api;
using UnityEngine;

public class AdManager : MonoBehaviour
{
    private BannerView bannerView;
    public string bannerID;
    public string appID;

    public static AdManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appID);

        RequestBanner();
    }

    public void Show()
    {
        bannerView.Show();
    }

    public void Hide()
    {
        bannerView.Hide();
    }

    private void RequestBanner()
    {
        bannerView = new BannerView(bannerID, AdSize.SmartBanner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder()
            //.AddTestDevice("C4B847B26F304503A93DAA31B89A2103")
            .Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }
}
