﻿using UnityEngine;
using System.Collections;
using System;

public class CameraController : MonoBehaviour
{
    public static bool isFollowingPlayer = true;
    public static bool foundPlayer = false;
    public static bool isUpdateDisabled = false;

    public GameObject player;
    public float followingSpeed;

    private readonly float distanceThreshold = 0.1f;
    private readonly float timeBeforeFollowing = 3f;
    private readonly float shakeDuration = 0.1f;
    private readonly float shakeMagnitude = 3f;

    private float yOffset = 3f;
    private float z = -10f;
    private Vector3 destination;

    private bool isTriggeringGates = true;
    private Gates gatesToOpen;
    private Coroutine cameraShaking;

    public static event Action End = delegate { };

    private void OnEnable()
    {
        PlayerController.OnRespawn += FollowPlayer;
        PlayerController.OnHit += ShakeCamera;
        PressurePlate.Pressed += ShowGatesOpen;
    }

    private void OnDisable()
    {
        PlayerController.OnRespawn -= FollowPlayer;
        PlayerController.OnHit -= ShakeCamera;
        PressurePlate.Pressed -= ShowGatesOpen;
    }

    // Use this for initialization
    void Start()
    {
        destination = new Vector3(player.transform.position.x, player.transform.position.y + yOffset, z);
        transform.position = destination;
        isFollowingPlayer = true;
        foundPlayer = false;
        isUpdateDisabled = false;
    }

    void LateUpdate()
    {
        if (!isUpdateDisabled)
        {
            if (isFollowingPlayer)
            {
                destination = new Vector3(player.transform.position.x, player.transform.position.y + yOffset, z);
                isTriggeringGates = true;
            }
            if (player != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * followingSpeed);
            }
            if (Vector3.Distance(transform.position, destination) < distanceThreshold)
            {
                if (isFollowingPlayer)
                {
                    foundPlayer = true;
                }
                else if (isTriggeringGates)
                {
                    isTriggeringGates = false;
                    StartCoroutine(OpenTheGates());
                }
            }
            else if (isFollowingPlayer)
            {
                foundPlayer = false;
            }
        }
        else if (!player.GetComponent<Renderer>().IsVisibleFrom(Camera.main))
        {
            End();
        }
    }

    private IEnumerator OpenTheGates()
    {
        gatesToOpen.OpenTheGates();
        yield return new WaitForSeconds(timeBeforeFollowing);
        FollowPlayer();
        transform.position = player.transform.position;
    }

    private void FollowPlayer()
    {
        isFollowingPlayer = true;
        destination = new Vector3(player.transform.position.x, player.transform.position.y + yOffset, z);
    }

    private void ShowGatesOpen(Gates gates)
    {
        foundPlayer = false;
        isFollowingPlayer = false;
        destination = new Vector3(gates.transform.position.x, gates.transform.position.y, z);
        gatesToOpen = gates;
    }

    private void ShakeCamera()
    {
        if(cameraShaking != null)
        {
            StopCoroutine(cameraShaking);
        }
        cameraShaking = StartCoroutine(Shake());
    }

    private IEnumerator Shake()
    {
        float elapsed = 0f;
        Vector3 originalCamPos = transform.position;

        while (elapsed < shakeDuration)
        {
            elapsed += Time.deltaTime;

            float percentComplete = elapsed / shakeDuration;
            float damper = 1f - Mathf.Clamp(4f * percentComplete - 3f, 0f, 1f);

            float x = transform.position.x ;
            float y = transform.position.y;
            x += shakeMagnitude * damper * UnityEngine.Random.Range(-1f, 1f);
            y += shakeMagnitude * damper * UnityEngine.Random.Range(-1f, 1f);

            transform.position = new Vector3(x, y, originalCamPos.z);

            yield return null;
        }
    }

    private void OnValidate()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
    }
}