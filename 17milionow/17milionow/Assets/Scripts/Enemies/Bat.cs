﻿using UnityEngine;

public class Bat : MonoBehaviour, IOnTouch, IDestroyable
{
    private const float LOSE_INTEREST_TIME = 5f;

    public float speed;
    public float detectionRange;
    public int damage;
    public float knockback;

    public GameObject sleepParticleHolder;

    private PlayerController player;

    private float chaseRange;

    private bool isSleeping = true;
    private bool isChasing = false;
    private bool isReturning = false;
    private bool isFlying = false;

    private Vector2 startPosition;
    private float timeCounter = 0f;
    private SpriteRenderer sprite;

    private readonly float minReturnDistance = 0.01f;

    public GameObject explosionParticle;
   
    private void Awake()
    {
        chaseRange = detectionRange / 1.25f;
    }

    // Use this for initialization
    void Start ()
    {
        startPosition = new Vector2(transform.position.x, transform.position.y);
        player = FindObjectOfType<PlayerController>();
        sprite = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!isChasing)
        {
            CheckDistanceToPlayer();
            if (isReturning)
            {
                if (Vector3.Distance(transform.position, new Vector3(startPosition.x, startPosition.y, transform.position.z)) < minReturnDistance)
                {
                    isReturning = false;
                    isFlying = false;
                    GetComponent<Animator>().SetTrigger("stop");
                }
                else
                    MoveTowardsDestination(startPosition);
            }
        }
        else
        {
            MoveTowardsDestination(player.transform.position);
            CheckInterest();
        }
	}

    private void OnEnable()
    {
        PlayerController.OnRespawn += Return;
    }

    private void OnDisable()
    {
        PlayerController.OnRespawn -= Return;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
        Gizmos.DrawWireSphere(transform.position, chaseRange);
    }

    private void CheckDistanceToPlayer()
    {
        if (isSleeping && Vector2.Distance(transform.position, player.transform.position) <= detectionRange)
        {
            isSleeping = false;
            GetComponent<Animator>().SetBool("isSleeping", false);
            Destroy(sleepParticleHolder);
        }
        else if (!isSleeping && Vector2.Distance(transform.position, player.transform.position) <= chaseRange)
        {
            isReturning = false;
            isChasing = true;
            if (!isFlying)
            {
                isFlying = true;
                GetComponent<Animator>().SetTrigger("chase");
            }
        }
    }

    public void OnTouch(PlayerController player)
    {
        player.GiveDamage(damage);
        player.KnockBack(knockback, transform.position);
    }

    private void CheckInterest()
    {
        timeCounter += Time.deltaTime;
        if (Vector2.Distance(transform.position, player.transform.position) <= detectionRange)
        {
            timeCounter = 0f;
        }
        else if(timeCounter >= LOSE_INTEREST_TIME)
        {
            isChasing = false;
            isReturning = true;
            timeCounter = 0f;
        }
    }

    private void Return()
    {
        if (isChasing)
        {
            isChasing = false;
            isReturning = true;
            timeCounter = 0f;
        }
    }

    private void MoveTowardsDestination(Vector2 destination)
    {
        if(destination.x > transform.position.x)
        {
            sprite.flipX = true;
        }
        else
        {
            sprite.flipX = false;
        }
        transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);
    }

    public void BeforeDestroy()
    {
        Instantiate(explosionParticle, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
