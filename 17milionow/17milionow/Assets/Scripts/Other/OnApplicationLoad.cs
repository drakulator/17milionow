﻿using UnityEngine;

public class OnApplicationLoad
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void OnBeforeSceneLoad()
    {
        Map[] maps = Resources.LoadAll<Map>("Maps");
        foreach (Map map in maps)
        {
            if (PlayerPrefs.HasKey(map.name))
            {
                if(PlayerPrefs.GetInt(map.name) == 1)
                {
                    map.isUnlocked = true;
                }
                if (PlayerPrefs.GetInt(map.name) == 2)
                {
                    map.isUnlocked = true;
                    map.isCompleted = true;
                }
            }
            if (PlayerPrefs.HasKey(map.name + "_Bows"))
            {
                map.bowsFound = PlayerPrefs.GetInt(map.name + "_Bows");
            }
        }
        if (!PlayerPrefs.HasKey("GlobalBowsFound"))
        {
            PlayerPrefs.SetInt("GlobalBowsFound", 0);
        }
    }
}
