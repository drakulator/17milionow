﻿using System.Collections.Generic;
using UnityEngine;

public static class IdsHolder
{
    public static HashSet<string> ids = new HashSet<string>();
    public static long lastId = 1000;

    public static string GenerateUniqueID(GameObject other)
    {
        string baseString = other.name + other.scene.name + "_";
        do
        {
            lastId++;
        } while (!ids.Add(baseString + lastId.ToString()));
        return baseString + lastId.ToString();
    }
}
