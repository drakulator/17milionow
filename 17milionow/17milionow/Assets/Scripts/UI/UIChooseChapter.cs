﻿using UnityEngine;
using UnityEngine.UI;

public class UIChooseChapter : MonoBehaviour
{
    public GameObject mainMenuHolder;
    public GameObject mapSelectionHolder;

    public Button swipeLeft;
    public Button swipeRight;
    public Button playButton;
    public Text chapterNameHolder;
    public Image chapterImageHolder;
    public Text bowsText;
    public Image previousChapterImage;
    public Image nextChapterImage;

    public Sprite swipeLeftFull;
    public Sprite swipeLeftEmpty;
    public Sprite swipeRightFull;
    public Sprite swipeRightEmpty;

    public Chapter currentChapter;
    public UIChooseMap chooseMap;

    // Use this for initialization
    void Start()
    {
        UpdateChapters();
    }

    public void UpdateChapters()
    {
        chapterNameHolder.text = currentChapter.chapterName;
        chapterImageHolder.sprite = currentChapter.chapterImage;
        int bowsFound = 0;
        int bowsMax = 0;
        foreach (Map map in currentChapter.maps)
        {
            bowsFound += map.bowsFound;
            bowsMax += map.bowsMax;
        }
        foreach (Map map in currentChapter.bonusMaps)
        {
            bowsFound += map.bowsFound;
            bowsMax += map.bowsMax;
        }

        bowsText.text = bowsFound + "/" + bowsMax;

        if (currentChapter.previousChapter != null)
        {
            swipeLeft.image.sprite = swipeLeftFull;
            previousChapterImage.gameObject.SetActive(true);
            previousChapterImage.sprite = currentChapter.previousChapter.chapterImage;
        }
        else
        {
            swipeLeft.image.sprite = swipeLeftEmpty;
            previousChapterImage.gameObject.SetActive(false);
        }

        if (currentChapter.nextChapter != null)
        {
            swipeRight.image.sprite = swipeRightFull;
            nextChapterImage.gameObject.SetActive(true);
            nextChapterImage.sprite = currentChapter.nextChapter.chapterImage;
        }
        else
        {
            swipeRight.image.sprite = swipeRightEmpty;
            nextChapterImage.gameObject.SetActive(false);
        }
    }

    public void B_Select()
    {
        mapSelectionHolder.SetActive(true);
        gameObject.SetActive(false);
        chooseMap.currentChapter = currentChapter;
        chooseMap.currentMap = currentChapter.startMap;
        chooseMap.UpdateMaps();
    }

    public void B_SwipeChapterLeft()
    {
        if (currentChapter.previousChapter != null)
        {
            currentChapter = currentChapter.previousChapter;
            chooseMap.currentChapter = currentChapter;
            chooseMap.currentMap = currentChapter.startMap;
            UpdateChapters();
            chooseMap.UpdateMaps();
        }
    }

    public void B_SwipeChapterRight()
    {
        if (currentChapter.nextChapter != null)
        {
            currentChapter = currentChapter.nextChapter;
            chooseMap.currentChapter = currentChapter;
            chooseMap.currentMap = currentChapter.startMap;
            UpdateChapters();
            chooseMap.UpdateMaps();
        }
    }

    public void B_Back()
    {
        gameObject.SetActive(false);
        mainMenuHolder.SetActive(true);
    }
}