﻿using UnityEngine;
using UnityEngine.UI;

public class UIChancesController : MonoBehaviour
{
    public Text chancesText;

    private void OnEnable()
    {
        PlayerController.OnChancesChanged += UpdateChances;
    }

    private void OnDisable()
    {
        PlayerController.OnChancesChanged -= UpdateChances;
    }

    private void UpdateChances(int chances)
    {
        chancesText.text = "x" + chances;
    }
}
