﻿using UnityEngine;
using UnityEngine.UI;

public class UIChooseMap : MonoBehaviour
{
    public GameObject chapterSelectionHolder;

    public Button swipeLeft;
    public Button swipeRight;
    public Button playButton;
    public Button playButton2;
    public Image playButtonImage;
    public Text chapterNameHolder;
    public Text mapNameHolder;
    public Image mapImageHolder;
    public Text bowsText;
    public GameObject starsPanel;
    public Image previousMapImage;
    public Image nextMapImage;

    public Sprite starFull;
    public Sprite starEmpty;
    public Sprite playFull;
    public Sprite playEmpty;
    public Sprite swipeLeftFull;
    public Sprite swipeLeftEmpty;
    public Sprite swipeRightFull;
    public Sprite swipeRightEmpty;

    public Chapter currentChapter;
    public Map currentMap;

    public GameObject loadingLevelHolder;

    // Use this for initialization
    void Start()
    {
        UpdateMaps();
    }

    public void UpdateMaps()
    {
        chapterNameHolder.text = currentChapter.chapterName;
        mapNameHolder.text = currentMap.mapName;
        bowsText.text = currentMap.bowsFound + "/" + currentMap.bowsMax;
        int iterator = 0;
        foreach (Image star in starsPanel.GetComponentsInChildren<Image>())
        {
            if (iterator < currentMap.difficulty)
            {
                star.sprite = starFull;
            }
            else
            {
                star.sprite = starEmpty;
            }
            iterator++;
        }
        if (currentMap.previousMap != null)
        {
            if (currentMap.previousMap.isUnlocked)
            {
                previousMapImage.sprite = currentMap.previousMap.mapImage;
            }
            else
            {
                previousMapImage.sprite = currentMap.previousMap.mapImageLocked;
            }
        }
        if (currentMap.nextMap != null)
        {
            if (currentMap.nextMap.isUnlocked)
            {
                nextMapImage.sprite = currentMap.nextMap.mapImage;
            }
            else
            {
                nextMapImage.sprite = currentMap.nextMap.mapImageLocked;
            }
        }

        if (currentMap.isUnlocked)
        {
            playButton.interactable = true;
            playButton2.interactable = true;
            playButtonImage.sprite = playFull;
            mapImageHolder.sprite = currentMap.mapImage;
        }
        else
        {
            playButton.interactable = false;
            playButton2.interactable = false;
            playButtonImage.sprite = playEmpty;
            mapImageHolder.sprite = currentMap.mapImageLocked;
        }

        if (currentMap.previousMap != null)
        {
            swipeLeft.image.sprite = swipeLeftFull;
            previousMapImage.gameObject.SetActive(true);
        }
        else
        {
            swipeLeft.image.sprite = swipeLeftEmpty;
            previousMapImage.gameObject.SetActive(false);
        }

        if (currentMap.nextMap != null)
        {
            swipeRight.image.sprite = swipeRightFull;
            nextMapImage.gameObject.SetActive(true);
        }
        else
        {
            swipeRight.image.sprite = swipeRightEmpty;
            nextMapImage.gameObject.SetActive(false);
        }
    }

    public void B_Play()
    {
        loadingLevelHolder.SetActive(true);
        loadingLevelHolder.GetComponent<UILoadingLevel>().StartCoroutine(loadingLevelHolder.GetComponent<UILoadingLevel>().AsynchronousLoad(currentMap.name));
        gameObject.SetActive(false);
    }

    public void B_SwipeLeft()
    {
        if (currentMap.previousMap != null)
        {
            currentMap = currentMap.previousMap;
            UpdateMaps();
        }
    }

    public void B_SwipeRight()
    {
        if (currentMap.nextMap != null)
        {
            currentMap = currentMap.nextMap;
            UpdateMaps();
        }
    }

    public void B_Back()
    {
        gameObject.SetActive(false);
        chapterSelectionHolder.SetActive(true);
    }
}