﻿using UnityEngine;
using UnityEngine.UI;

public class UIPumpMeter : MonoBehaviour
{
    public Slider slider;

    private void Start()
    {
        //slider.maxValue = PlayerController.MAX_STAGE;
    }

    private void OnEnable()
    {
        PlayerController.OnStageChanged += UpdateSlider;
    }

    private void OnDisable()
    {
        PlayerController.OnStageChanged -= UpdateSlider;
    }

    private void UpdateSlider(int stage)
    {
        slider.value = stage - 1;
    }
}
