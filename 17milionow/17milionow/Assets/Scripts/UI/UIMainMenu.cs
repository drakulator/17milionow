﻿using UnityEngine;

public class UIMainMenu : MonoBehaviour
{
    public static bool isLoadingChooseScene = false;
    public static Map currentMap;
    public static Chapter currentChapter;

    public GameObject chapterSelectionHolder;
    public GameObject mapSelectionHolder;
    public GameObject optionsHolder;

    private void Start()
    {
        AdManager.Instance.Show();
        if (isLoadingChooseScene)
        {
            isLoadingChooseScene = false;
            gameObject.SetActive(false);
            mapSelectionHolder.SetActive(true);
            if (currentChapter != null)
            {
                mapSelectionHolder.GetComponent<UIChooseMap>().currentChapter = currentChapter;
                if (currentMap != null)
                    mapSelectionHolder.GetComponent<UIChooseMap>().currentMap = currentMap;
            }
        }
    }

    public void B_Play()
    {
        gameObject.SetActive(false);
        chapterSelectionHolder.SetActive(true);
    }

    public void B_Achivements()
    {
        Debug.Log("not implemented yet");
    }

    public void B_Options()
    {
        gameObject.SetActive(false);
        optionsHolder.SetActive(true);
    }

    public void B_Quit()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void B_OpenFB()
    {
        Application.OpenURL("https://www.facebook.com/FluffyLoves/");
    }
}
