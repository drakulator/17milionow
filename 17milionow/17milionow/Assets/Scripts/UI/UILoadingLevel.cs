﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILoadingLevel : MonoBehaviour
{
    public Slider loadingProgressSlider;
    public Text loadingProgressText;

    public IEnumerator AsynchronousLoad(string scene)
    {
        yield return null;

        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);
        operation.allowSceneActivation = false;

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            Debug.Log("Loading progress: " + (int)(progress * 100) + "%");
            loadingProgressSlider.value = (int)(progress * 100);
            loadingProgressText.text = (int)(progress * 100) + "%";

            if (progress >= 0.9f)
            {
                operation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
