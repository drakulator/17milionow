﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIGamePanels : MonoBehaviour
{
    public static bool isPanelOpen = false;
    public static bool isGameEnding = false;

    public GameObject signPanelHolder;
    public GameObject gameOverPanelHolder;
    public GameObject settingsPanelHolder;
    public GameObject endingPanelHolder;
    public Text signText;

    private void Start()
    {
        isPanelOpen = false;
        isGameEnding = false;
    }

    private void OnEnable()
    {
        Sign.Opened += ShowSign;
        PlayerController.GameOver += GameOver;
    }

    private void OnDisable()
    {
        Sign.Opened -= ShowSign;
        PlayerController.GameOver -= GameOver;
    }

    private void ShowSign(string text)
    {
        if (!isGameEnding && !isPanelOpen)
        {
            signText.text = text;
            signPanelHolder.SetActive(true);
            isPanelOpen = true;
            Time.timeScale = 0f;
        }
    }

    public void B_Sign_Quit()
    {
        signPanelHolder.SetActive(false);
        isPanelOpen = false;
        Time.timeScale = 1f;
    }

    public void B_Show_Settings()
    {
        if (!isGameEnding)
        {
            if (!isPanelOpen)
            {
                settingsPanelHolder.SetActive(true);
                isPanelOpen = true;
                Time.timeScale = 0f;
            }
            else if (settingsPanelHolder.activeSelf)
            {
                settingsPanelHolder.SetActive(false);
                isPanelOpen = false;
                Time.timeScale = 1f;
            }
        }
    }

    private void ShowGameOverPanel()
    {
        gameOverPanelHolder.SetActive(true);
        isPanelOpen = true;
        Time.timeScale = 0f;
    }

    public void ShowEndingPanel()
    {
        endingPanelHolder.SetActive(true);
        isPanelOpen = true;
    }

    public void B_Resume()
    {
        settingsPanelHolder.SetActive(false);
        isPanelOpen = false;
        Time.timeScale = 1f;
    }

    public void B_Quit()
    {
        isPanelOpen = false;
        Time.timeScale = 1f;
        UIMainMenu.isLoadingChooseScene = true;
        UIMainMenu.currentChapter = FindObjectOfType<LevelManager>().CurrentMap.Chapter;
        UIMainMenu.currentMap = FindObjectOfType<LevelManager>().CurrentMap;
        SceneManager.LoadScene("MainMenu");
    }

    public void B_Restart()
    {
        isPanelOpen = false;
        isGameEnding = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void GameOver()
    {
        isGameEnding = true;
        StartCoroutine(OnGameOver());
    }

    private IEnumerator OnGameOver()
    {
        yield return new WaitForSeconds(2f);
        ShowGameOverPanel();
    }
}
