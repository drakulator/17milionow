﻿using UnityEngine;
using UnityEngine.UI;

public class UILivesController : MonoBehaviour
{
    public Sprite fullHeart;
    public Sprite halfHeart;
    public Sprite emptyHeart;

    private Image[] hearts;

    private void Start()
    {
        hearts = GetComponentsInChildren<Image>();
    }

    private void OnEnable()
    {
        PlayerController.OnLivesChanged += UpdateLives;
    }

    private void OnDisable()
    {
        PlayerController.OnLivesChanged -= UpdateLives;
    }

    private void UpdateLives(int health)
    {
        int remainingLives = health;
        foreach (Image heart in hearts)
        {
            if(remainingLives > 1)
            {
                heart.sprite = fullHeart;
                remainingLives -= 2;
            }
            else if (remainingLives == 1)
            {
                heart.sprite = halfHeart;
                remainingLives --;
            }
            else
            {
                heart.sprite = emptyHeart;
            }
        }
    }
}
