﻿using UnityEngine;

public class UIOptions : MonoBehaviour
{
    public GameObject mainMenuHolder;
    public GameObject creditsHolder;

    public void B_Back()
    {
        creditsHolder.SetActive(false);
        mainMenuHolder.SetActive(true);
        gameObject.SetActive(false);
    }

    public void B_OpenFB()
    {
        Application.OpenURL("https://www.facebook.com/FluffyLoves/");
    }

    public void B_OpenCredits()
    {
        creditsHolder.SetActive(true);
    }

    public void B_CloseCredits()
    {
        creditsHolder.SetActive(false);
    }
}
