﻿using UnityEngine;
using NaughtyAttributes;

public class Gates : MonoBehaviour
{
    [Button]
    public void OpenTheGates()
    {
        GetComponent<Animator>().SetTrigger("open");
    }
}
