﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpongeActive : MonoBehaviour, IClickable
{
    private const float MAX_TIME = 3f;

    [SerializeField]
    private GameObject sliderPanel;
    [SerializeField]
    private Slider slider;

    public float force = 1000f;

    private bool isTapped = false;
    private float timeTouching = 0f;
    private List<Collider2D> colliders;

    private void Start()
    {
        //slider.maxValue = MAX_TIME;
        colliders = new List<Collider2D>();
    }

    private void Update()
    {
        if (isTapped)
        {
            timeTouching += Time.unscaledDeltaTime;
            slider.value = timeTouching;
            if(Input.GetTouch(0).phase == TouchPhase.Ended || timeTouching > MAX_TIME)
            {
                sliderPanel.SetActive(false);
                isTapped = false;
                ThrowObjects();
            }
        }
    }

    public void Clicked()
    {
        timeTouching = 0f;
        sliderPanel.SetActive(true);
        isTapped = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        colliders.Add(collision.collider);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (colliders.Contains(collision.collider))
            colliders.Remove(collision.collider);
    }

    private void ThrowObjects()
    {
        foreach (Collider2D collider in colliders)
        {
            if (collider != GetComponent<Collider2D>())
            {
                if (collider.GetComponent<Rigidbody2D>() != null)
                {
                    Vector2 directionVector = new Vector2((collider.transform.position.x - transform.position.x) * 0.1f,
                        collider.transform.position.y - transform.position.y + 10f);
                    collider.GetComponent<Rigidbody2D>().AddForce(directionVector * force * timeTouching, ForceMode2D.Impulse);
                }
            }
        }
    }

    private void OnValidate()
    {
        if(sliderPanel == null)
        {
            sliderPanel = GameObject.Find("Canvas").transform.Find("SpongeMeter").gameObject;
        }
        if(slider == null)
        {
            slider = GameObject.Find("Canvas").transform.Find("SpongeMeter").transform.Find("Slider").GetComponent<Slider>();
        }
    }
}
