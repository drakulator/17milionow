﻿using NaughtyAttributes;
using System;
using UnityEngine;

public class Sign : MonoBehaviour, IClickable
{
    [SerializeField]
    [ResizableTextArea]
    private string text;
    public GameObject questionMarkParticleHolder;

    public string id;

    public static event Action<string> Opened = delegate { };

    private void Start()
    {
        if (PlayerPrefs.HasKey(id))
        {
            Destroy(questionMarkParticleHolder);
        }
    }

#if UNITY_EDITOR
    private void OnEnable()
    {
        AssignId();
    }
#endif

    private void Awake()
    {
        AssignId();
    }

    public void Clicked()
    {
        Destroy(questionMarkParticleHolder);
        PlayerPrefs.SetInt(id, 1);
        Opened(text);
    }

    private void AssignId()
    {
        if (string.IsNullOrEmpty(id))
            id = IdsHolder.GenerateUniqueID(gameObject);
    }

    [Button]
    private void ResetId()
    {
        id = IdsHolder.GenerateUniqueID(gameObject);
    }

    [Button]
    private void Show()
    {
        Opened(text);
    }
}
