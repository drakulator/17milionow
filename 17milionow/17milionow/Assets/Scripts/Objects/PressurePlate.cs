﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    public Gates gatesToOpen;
    public float massRequired;
    public GameObject defaultHolder;
    public GameObject pressedHolder;

    private Collider2D[] potentialColliders;
    private List<Collider2D> colliders;
    private bool isPressed = false;

    public static event Action<Gates> Pressed = delegate { };

    private void OnCollisionEnter2D(Collision2D collision)
    {
        colliders.Add(collision.collider);
        CheckMassAttached();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (colliders.Contains(collision.collider))
            colliders.Remove(collision.collider);
    }

    // Use this for initialization
    void Start () {
        colliders = new List<Collider2D>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position + 6.5f * Vector3.up * transform.lossyScale.y,
            new Vector3(18f * transform.lossyScale.x, 10f * transform.lossyScale.y));
    }

    private void Update()
    {
        potentialColliders = Physics2D.OverlapBoxAll(transform.position + 6.5f * Vector3.up * transform.lossyScale.y,
            new Vector3(18f * transform.lossyScale.x, 10f * transform.lossyScale.y), 0f);
        if (potentialColliders.Length > 0 && !isPressed)
        {
            CheckMassAttached();
        }
    }

    private void CheckMassAttached()
    {
        float mass = 0f;
        if(colliders.Count > 0)
        {
            foreach (Collider2D collider in colliders)
            {
                if (collider != GetComponent<Collider2D>())
                {
                    if (collider.GetComponent<Rigidbody2D>() != null)
                    {
                        mass += collider.GetComponent<Rigidbody2D>().mass;
                    }
                }
            }
        }
        if (potentialColliders.Length > 0)
        {
            foreach (Collider2D collider in potentialColliders)
            {
                if (collider != GetComponent<Collider2D>() && !(colliders.Contains(collider)))
                {
                    if (collider.GetComponent<Rigidbody2D>() != null && collider.GetComponent<Rigidbody2D>().velocity.magnitude < 0.01f)
                    {
                        mass += collider.GetComponent<Rigidbody2D>().mass;
                    }
                }
            }
        }
        if (mass > massRequired)
        {
            isPressed = true;
            Pressed(gatesToOpen);
            MusicPlayer.Instance.pressurePlateSound.Play();
            pressedHolder.SetActive(true);
            defaultHolder.SetActive(false);
        }
    }
}
