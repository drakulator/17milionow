﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using System.Collections;

public class InputHandler : MonoBehaviour
{
    public static event Action<float> OnAccelerate = delegate { };
    public static event Action OnScreenTapped = delegate { };

    private readonly float maxAccelerationAbs = 0.32f;

    [SerializeField]
    [Slider(-0.32f, 0.32f)]
    private float acceleration;

    void Update()
    {
        if (CameraController.foundPlayer && !LevelManager.isMoveFrozen && (!UIGamePanels.isPanelOpen || LevelManager.isGameEnding))
        {
            if (Input.acceleration.x != 0f)
            {
                OnAccelerate(Mathf.Clamp(Input.acceleration.x, -maxAccelerationAbs, maxAccelerationAbs) * 1.5f);
            }

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (!IsUIHit())
                {
                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);
                    if (hit)
                    {
                        IClickable clickableObject = hit.transform.gameObject.GetComponent<IClickable>();
                        if (clickableObject != null)
                            clickableObject.Clicked();
                        else
                            OnScreenTapped();
                    }
                    else
                    {
                        OnScreenTapped();
                    }
                }
            }
        }
    }

    private bool IsUIHit()
    {
        return EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
    }

    [Button]
    private void Accelerate()
    {
        StartCoroutine(AccelerateEnum());
    }

    private IEnumerator AccelerateEnum()
    {
        float time = 0f;
        while(time <= 2f)
        {
            Debug.Log(acceleration);
            OnAccelerate(acceleration * 1.5f);
            time += Time.unscaledDeltaTime;
            yield return null;
        }
    }
}

