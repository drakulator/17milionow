﻿using System;
using System.Collections;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    public const int MAX_STAGE = 10;
    public const int MAX_HEALTH = 6;
    public const int START_CHANCES = 2;

    public float speed;
    public GameObject damageParticle;
    public GameObject explosionParticle;

    public int Health { get; private set; }
    public int Chances { get; private set; }
    private int stage = 1;
    private float timeToPumpOutLeft;
    private bool isPumpingOut;
    private Vector2 checkpoint;

    private readonly float floatForce = 12000f;
    private readonly float stageSpeedMultiplier = 0.3f;
    private readonly float timeToPumpOut = 1.5f;
    private readonly float timeBetweenPositionTransmits = 0.25f;
    private readonly float gravityScaleModifier = 0.3f;

    public static event Action<Vector2> TransmitPositionAction = delegate { };
    public static event Action<int> OnLivesChanged = delegate { };
    public static event Action<int> OnStageChanged = delegate { };
    public static event Action<int> OnChancesChanged = delegate { };
    public static event Action OnHit = delegate { };
    public static event Action OnRespawn = delegate { };
    public static event Action GameOver = delegate { };

    private ContactPoint2D[] lastCollisionContacts;
    private Animator animator;

    private float startGravityScale;
    private float startColliderRadius;

    private void Start()
    {
        Health = MAX_HEALTH;
        Chances = START_CHANCES;
        animator = GetComponent<Animator>();
        checkpoint = transform.position;
        OnChancesChanged(Chances);

        startGravityScale = GetComponent<Rigidbody2D>().gravityScale;
        startColliderRadius = GetComponent<CircleCollider2D>().radius;

        InvokeRepeating("TransmitPosition", timeBetweenPositionTransmits, timeBetweenPositionTransmits);
    }

    private void OnEnable()
    {
        InputHandler.OnAccelerate += Move;
        InputHandler.OnScreenTapped += OnTap;
    }

    private void OnDisable()
    {
        InputHandler.OnAccelerate -= Move;
        InputHandler.OnScreenTapped -= OnTap;
    }

    private void TransmitPosition()
    {
        TransmitPositionAction(transform.position);
    }

    private void FixedUpdate()
    {
        if(stage > 1)
        {
            timeToPumpOutLeft -= Time.deltaTime;
            if (timeToPumpOutLeft <= 0f && !isPumpingOut)
                PumpOut();
        }
        else
        {
            bool isRolling = Mathf.Abs(GetComponent<Rigidbody2D>().angularVelocity) > 10f;
            animator.SetBool("isRolling", isRolling);
        }
    }

    private void Move(float input)
    {
        Vector2 forceVector = Vector2.right * input * speed;
        GetComponent<Rigidbody2D>().AddForce(forceVector / (stage * stageSpeedMultiplier), ForceMode2D.Impulse);
    }

    [Button]
    private void OnTap()
    {
        if(stage < MAX_STAGE)
        {
            PumpUp();
        }
        else if(!LevelManager.isTriggeringGameEnding)
        {
            OnStageChanged(MAX_STAGE + 1);
            OnDeath();
        }
    }

    private void PumpUp()
    {
        stage++;
        GetComponent<Rigidbody2D>().gravityScale -= gravityScaleModifier;
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * floatForce, ForceMode2D.Force);
        timeToPumpOutLeft = timeToPumpOut;
        OnStageChanged(stage);
        if (stage == 2)
            animator.Play("Player_Pumping", 0, 0f);
        if (stage >= 2)
            animator.SetFloat("pumpingTime", (1f / MAX_STAGE) * (stage - 1f));
        animator.SetInteger("stage", stage);
        animator.ResetTrigger("hit");

        if(UnityEngine.Random.value > 0.8)
        {
            MusicPlayer.Instance.pumpingSound.Play();
        }

        Debug.Log("Stage = " + stage);  
    }

    private void PumpOut()
    {
        stage--;
        GetComponent<Rigidbody2D>().gravityScale += gravityScaleModifier;
        OnStageChanged(stage);
        animator.SetFloat("pumpingTime", (1f / MAX_STAGE) * (stage - 1f));
        animator.SetInteger("stage", stage);
        animator.ResetTrigger("hit");

        StartCoroutine(WaitBetweenPumpingOuts());
        Debug.Log("Stage = " + stage);
    }

    private IEnumerator WaitBetweenPumpingOuts()
    {
        isPumpingOut = true;
        yield return new WaitForSeconds(0.1f);
        isPumpingOut = false;
    }

    public void KnockBack(float knockback, Vector3 position)
    {
        Vector2 direction = (transform.position - position).normalized;
        GetComponent<Rigidbody2D>().AddForce(direction * knockback, ForceMode2D.Impulse);
    }

    public void GiveDamage(int damage, bool isShowingImpact = true)
    {
        if (!LevelManager.isTriggeringGameEnding)
        {
            if (damage > 0)
            {
                if (isShowingImpact)
                {
                    foreach (ContactPoint2D contact in lastCollisionContacts)
                    {
                        Instantiate(damageParticle, contact.point, Quaternion.identity);
                    }
                }
                Health -= damage;
                if (Health <= 0)
                    OnDeath();
                OnLivesChanged(Health);
                animator.SetTrigger("hit");
                OnHit();
                Handheld.Vibrate();
                MusicPlayer.Instance.hurtSound.Play();
                MusicPlayer.Instance.hitSound.Play();
            }
            else
            {
                Debug.Log("Negative damage received!");
            }
        }
    }

    public void Heal(int healing)
    {
        if (healing > 0)
        {
            if (Health < MAX_HEALTH)
            {
                Health += healing;
                OnLivesChanged(Health);
            }
        }
        else
        {
            Debug.Log("Negative healing received!");
        }
    }

    private void OnDeath()
    {
        Health = 0;
        OnLivesChanged(Health);
        Instantiate(explosionParticle, transform.position, Quaternion.identity);
        MusicPlayer.Instance.popSound.Play();
        gameObject.SetActive(false);
        if(Chances > 0)
        {
            Respawn();
        }
        else
        {
            GameOver();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.GetComponent<IOnTouch>() != null)
        {
            lastCollisionContacts = collision.contacts;
            collision.collider.GetComponent<IOnTouch>().OnTouch(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<IOnTouch>() != null)
        {
            other.GetComponent<IOnTouch>().OnTouch(this);
        }
    }

    public void OnCheckpointCollected(Vector2 checkpoint)
    {
        this.checkpoint = checkpoint;
    }

    public void IncreaseChances()
    {
        Chances++;
        OnChancesChanged(Chances);
    }

    private void Respawn()
    {
        Chances--;
        OnChancesChanged(Chances);
        Health = MAX_HEALTH;
        OnLivesChanged(Health);
        transform.position = checkpoint;
        StopAllCoroutines();
        isPumpingOut = false;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().gravityScale = startGravityScale;
        GetComponent<CircleCollider2D>().radius = startColliderRadius;
        stage = 1;
        OnStageChanged(stage);
        animator.SetInteger("stage", stage);
        animator.ResetTrigger("hit");
        gameObject.SetActive(true);
        OnRespawn();
    }
}
