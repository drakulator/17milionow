﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public int bowsOnScene;
    public int BowsFound { get; private set; }

    private List<string> bowsIds = new List<string>();
    public Map CurrentMap { get; private set; }

    public static bool isMoveFrozen = false;
    public static bool isGameEnding = false;
    public static bool isTriggeringGameEnding = false;

    private void Awake()
    {
        Time.timeScale = 1f;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void Start()
    {
        AdManager.Instance.Hide();
        CurrentMap = Resources.Load<Map>("Maps/" + SceneManager.GetActiveScene().name);
        BowsFound = CurrentMap.bowsFound;
        isMoveFrozen = false;
        isGameEnding = false;
        isTriggeringGameEnding = false;
}

    private void OnValidate()
    {
        bowsOnScene = FindObjectsOfType<Bow>().Length;
    }

    private void OnEnable()
    {
        Bow.BowCollected += OnBowCollected;
        PinkBallController.LevelCompleted += OnLevelCompleted;
        CameraController.End += OnExit;
    }

    private void OnDisable()
    {
        Bow.BowCollected -= OnBowCollected;
        PinkBallController.LevelCompleted -= OnLevelCompleted;
        CameraController.End -= OnExit;
    }

    private void OnBowCollected(string id)
    {
        BowsFound++;
        bowsIds.Add(id);
    }

    private void OnLevelCompleted()
    {
        isMoveFrozen = true;
        isTriggeringGameEnding = true;
        StartCoroutine(WaitBeforeEnablingMovement());
    }

    private IEnumerator WaitBeforeEnablingMovement()
    {
        yield return new WaitForSeconds(5f);
        isMoveFrozen = false;
        CameraController.isUpdateDisabled = true;
        FindObjectOfType<UIGamePanels>().ShowEndingPanel();
        isGameEnding = true;
    }

    private void OnExit()
    {
        if (CurrentMap.bowsFound < BowsFound)
        {
            PlayerPrefs.SetInt(CurrentMap.name + "_Bows", BowsFound);
        }
        if (!CurrentMap.isCompleted)
        {
            PlayerPrefs.SetInt(CurrentMap.name, 2);
            CurrentMap.isCompleted = true;
            if (CurrentMap.nextMap != null && !CurrentMap.nextMap.isBonusMap)
            {
                PlayerPrefs.SetInt(CurrentMap.nextMap.name, 1);
                CurrentMap.nextMap.isUnlocked = true;
            }
        }
        if (bowsIds != null)
        {
            foreach (string id in bowsIds)
            {
                PlayerPrefs.SetInt(id, 1);
                PlayerPrefs.SetInt("GlobalBowsFound", PlayerPrefs.GetInt("GlobalBowsFound") + 1);
            }
        }
        PlayerPrefs.Save();
        UIMainMenu.isLoadingChooseScene = true;
        if (CurrentMap.nextMap != null)
        {
            UIMainMenu.currentChapter = CurrentMap.Chapter;
            UIMainMenu.currentMap = CurrentMap.nextMap;
        }
        else if (CurrentMap.Chapter.nextChapter != null)
        {
            UIMainMenu.currentChapter = CurrentMap.Chapter.nextChapter;
            UIMainMenu.currentMap = CurrentMap.Chapter.nextChapter.startMap;
        }
        else
        {
            UIMainMenu.currentChapter = CurrentMap.Chapter;
            UIMainMenu.currentMap = CurrentMap;
        }
        SceneManager.LoadScene("MainMenu");
    }
}