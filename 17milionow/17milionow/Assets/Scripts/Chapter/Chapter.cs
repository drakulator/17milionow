﻿using UnityEngine;
using NaughtyAttributes;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Chapter")]
public class Chapter : ScriptableObject
{
    public string chapterName;
    public Map startMap;
    [ReadOnly]
    public List<Map> maps;
    [ReadOnly]
    public List<Map> bonusMaps;
    public Chapter previousChapter;
    public Chapter nextChapter;
    public Sprite chapterImage;

    private void OnValidate()
    {
        maps = new List<Map>();
        bonusMaps = new List<Map>();
        if (startMap != null)
        {
            Map current = startMap;
            maps.Add(current);
            while (current.nextMap != null)
            {
                current = current.nextMap;
                if(current.isBonusMap)
                    bonusMaps.Add(current);
                else
                    maps.Add(current);
            }
        }
    }

    [Button]
    public void UpdateMaps()
    {
        maps = new List<Map>();
        bonusMaps = new List<Map>();
        if (startMap != null)
        {
            Map current = startMap;
            maps.Add(current);
            while (current.nextMap != null)
            {
                current = current.nextMap;
                if (current.isBonusMap)
                    bonusMaps.Add(current);
                else
                    maps.Add(current);
            }
        }
    }

    public bool IsNextChapterUnlocked()
    {
        return maps[maps.Count - 1].isCompleted;
    }
}
