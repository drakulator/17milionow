﻿public interface IDestroyable
{
    void BeforeDestroy();
}

public interface IDestroyableExplosive : IDestroyable
{

}

