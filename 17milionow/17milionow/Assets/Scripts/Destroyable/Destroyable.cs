﻿using UnityEngine;

public class Destroyable : MonoBehaviour, IDestroyable
{
    public GameObject destroyEffect;

    [SerializeField]
    private float destroyThreshold;

    public void BeforeDestroy()
    {
        Instantiate(destroyEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    public float GetThreshold()
    {
        return destroyThreshold;
    }
}
