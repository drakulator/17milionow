﻿using UnityEngine;
using System.Collections;

public class DestroyableWall : MonoBehaviour, IDestroyable
{
    public GameObject destroyEffect;

    [SerializeField]
    private float destroyThreshold = 240f;
    [SerializeField]
    private GameObject hiddenSection;

    public AudioSource destroyWallSound;

    private bool isFadingOut = false;

    private readonly float timeToFade = 3f;
    private readonly float breakForce = 250f;
    private readonly float breakRadius = 25f;

    private void Update()
    {
        if (isFadingOut)
        {
            foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
            {
                sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, sprite.color.a - Time.deltaTime / timeToFade);
            }
        }
    }

    private void Start()
    {
        foreach (Rigidbody2D rock in GetComponentsInChildren<Rigidbody2D>())
        {
            rock.isKinematic = true;
        }
    }

    public void BeforeDestroy()
    {
        Instantiate(destroyEffect, transform.position, transform.rotation);
        destroyWallSound.Play();
        foreach (Rigidbody2D rock in GetComponentsInChildren<Rigidbody2D>())
        {
            rock.isKinematic = false;
            rock.AddExplosionForce(breakForce, transform.position, breakRadius);
        }
        GetComponent<Collider2D>().enabled = false;
        isFadingOut = true;
        if (hiddenSection != null)
            Destroy(hiddenSection);
        StartCoroutine(DestroyParts());
    }

    private IEnumerator DestroyParts()
    {
        yield return new WaitForSeconds(timeToFade);
        Destroy(gameObject);
    }

    public float GetThreshold()
    {
        return destroyThreshold;
    }
}
