﻿using UnityEngine;

public class DamageOnTouch : MonoBehaviour, IOnTouch
{
    public int damage;
    public float knockback;

    public void OnTouch(PlayerController player)
    {
        player.GiveDamage(damage);
        player.KnockBack(knockback, transform.position);
    }
}
