﻿using System;
using UnityEngine;

public class PinkBallController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    private bool gameEnded = false;

    public GameObject heartsParticle;

    public static event Action LevelCompleted = delegate { };

    private void OnValidate()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
    }

    // Update is called once per frame
    void Update () {
		if(Vector2.Distance(transform.position, player.transform.position) < 20f && !gameEnded)
        {
            gameEnded = true;
            Instantiate(heartsParticle, transform.position, Quaternion.identity);
            GetComponent<Animator>().SetTrigger("endGame");
            LevelCompleted();
        }
	}
}
